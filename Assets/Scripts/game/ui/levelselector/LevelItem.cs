﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace game.ui.levelselector
{
    public class LevelItem : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private RawImage coverImage;
        [SerializeField] private TextMeshProUGUI itemNameText;

        public Action OnClickAction;

        private void Awake()
        {
            Debug.Assert(itemNameText != null, $"{nameof(itemNameText)} is null!");
        }

        public void SetItemName(string itemName)
        {
            itemNameText.text = itemName;
        }

        public void SetCoverImage(Texture2D texture2D)
        {
            coverImage.texture = texture2D;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClickAction?.Invoke();
        }
    }
}
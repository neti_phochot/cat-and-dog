﻿using System.Collections.Generic;
using game.level;
using game.world;
using game.world.map;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace game.ui.levelselector
{
    public class LevelMenuUIManager : MonoBehaviour
    {
        [Header("LEVEL")] [SerializeField] private MapSO mapSo;
        [SerializeField] private World.WorldData defaultWorldData;
        [SerializeField] private WorldDataLoader worldDataLoader;

        [Header("UI")] [SerializeField] private LevelSelectorUI levelSelectorUI;
        [Header("BUTTON")] [SerializeField] private Button playButton;
        [SerializeField] private Button exitButton;

        [Header("SETUP")] [SerializeField] private LevelItem levelItemTemplate;
        [SerializeField] private Transform levelItemContainer;

        private void Awake()
        {
            Debug.Assert(playButton != null, $"{nameof(playButton)} is null!");
            Debug.Assert(exitButton != null, $"{nameof(exitButton)} is null!");

            playButton.onClick.AddListener(levelSelectorUI.Show);
            exitButton.onClick.AddListener(Application.Quit);

            levelSelectorUI.Hide();
        }

        private void Start()
        {
            InitMap();
        }

        private void InitMap()
        {
            //Clear map
            mapSo.selectedWorldData.worldRawData = null;

            List<World.WorldData> levelItemDatas = worldDataLoader.LoadWorldData();

            //Check if we found custom map
            if (levelItemDatas.Count == 0)
            {
                //Custom map not found, load default one
                LevelItem levelItem = Instantiate(levelItemTemplate, levelItemContainer);
                levelItem.SetItemName(defaultWorldData.worldName);
                levelItem.OnClickAction = () => { LoadLevel(defaultWorldData); };
                levelItemTemplate.gameObject.SetActive(false);
                return;
            }

            foreach (var worldData in levelItemDatas)
            {
                LevelItem levelItem = Instantiate(levelItemTemplate, levelItemContainer);
                levelItem.SetItemName(worldData.worldName);
                levelItem.SetCoverImage(worldData.worldRawData);
                levelItem.OnClickAction = () => { LoadLevel(worldData); };
            }

            levelItemTemplate.gameObject.SetActive(false);
        }

        private void LoadLevel(World.WorldData worldData)
        {
            mapSo.selectedWorldData = worldData;
            SceneManager.LoadScene(1);
        }
    }
}
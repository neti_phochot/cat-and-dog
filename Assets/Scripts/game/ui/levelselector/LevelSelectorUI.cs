﻿using UnityEngine;
using UnityEngine.UI;

namespace game.ui.levelselector
{
    public class LevelSelectorUI : BaseUI
    {
        [SerializeField] private Button backButton;

        private void Awake()
        {
            Debug.Assert(backButton != null, $"{nameof(backButton)} is null!");
            
            backButton.onClick.AddListener(Hide);
        }
    }
}
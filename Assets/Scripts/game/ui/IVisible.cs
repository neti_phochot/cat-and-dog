﻿namespace game.ui
{
    public interface IVisible
    {
        void Show();
        void Hide();
    }
}
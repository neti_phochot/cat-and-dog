﻿using UnityEngine;

namespace game.ui.game
{
    public class GameUIManager : MonoBehaviour
    {
        public static GameUIManager Instance;
        [SerializeField] private GameController gameController;
        [Header("UI")] [SerializeField] private PausedMenuUI pausedMenuUI;
        [SerializeField] private GameUI gameUI;
        [SerializeField] private GameResultUI gameResultUI;

        private bool _isPaused;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;

            Debug.Assert(gameResultUI != null, $"{nameof(GameResultUI)} is null!");
            Debug.Assert(gameUI != null, $"{nameof(GameUI)} is null!");

            pausedMenuUI.Hide();
            gameResultUI.Hide();
        }

        private void Start()
        {
            gameResultUI.Hide();
        }

        public void SetMapName(string mapName)
        {
            gameUI.SetMapName(mapName);
        }

        public void SetScore(int score)
        {
            gameUI.SetScore(score);
        }

        public void SetFish(int current, int total)
        {
            gameUI.SetFish(current, total);
        }

        public void SetGameResult(bool win)
        {
            gameResultUI.SetWin(win);
            gameResultUI.Show();
        }

        public void SetGameTimer(float timer)
        {
            gameUI.SetTime(timer);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Q))
            {
                TogglePauseMenu();
            }
        }

        private void TogglePauseMenu()
        {
            if (_isPaused) pausedMenuUI.Hide();
            else if (!gameController.IsGameOver)
            {
                pausedMenuUI.Show();
            }

            _isPaused = !_isPaused;
        }
    }
}
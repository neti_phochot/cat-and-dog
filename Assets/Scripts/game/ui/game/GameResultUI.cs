﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using utils;

namespace game.ui.game
{
    public class GameResultUI : BaseUI
    {
        [Header("BUTTON")] [SerializeField] private Button restartButton;
        [SerializeField] private Button mainMenuButton;

        [Header("TEXT")] [SerializeField] private TextMeshProUGUI winText;
        [SerializeField] private TextMeshProUGUI lossText;

        [Header("BLINK EFFECT")] [SerializeField]
        private Transform blinkTransform;

        [SerializeField] private float blinkSpeed = 5f;

        private float _startTime;

        private void Awake()
        {
            restartButton.onClick.AddListener(RestartGame);
            mainMenuButton.onClick.AddListener(() => { SceneManager.LoadScene(0); });
        }

        private void RestartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public override void Show()
        {
            base.Show();
            _startTime = Time.realtimeSinceStartup;
            FreezeGame.Freeze(0.1f);
        }

        private void OnDisable()
        {
            FreezeGame.UnFreeze();
        }

        private void Update()
        {
            BlinkText();

            if (Input.GetButtonDown("Jump"))
            {
                RestartGame();
            }
        }

        public void SetWin(bool win)
        {
            winText.gameObject.SetActive(win);
            lossText.gameObject.SetActive(!win);
        }

        private void BlinkText()
        {
            float t = Time.realtimeSinceStartup - _startTime;
            bool blink = Mathf.Sin(t * blinkSpeed) > 0;
            blinkTransform.gameObject.SetActive(blink);
        }
    }
}
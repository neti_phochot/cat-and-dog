﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using utils;

namespace game.ui.game
{
    public class PausedMenuUI : BaseUI
    {
        [Header("BUTTON")] [SerializeField] private Button restartButton;
        [SerializeField] private Button continueButton;
        [SerializeField] private Button mainMenuButton;

        private void Awake()
        {
            restartButton.onClick.AddListener(() =>
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            });
            continueButton.onClick.AddListener(Hide);

            mainMenuButton.onClick.AddListener(() => { SceneManager.LoadScene(0); });
        }

        public override void Show()
        {
            base.Show();
            FreezeGame.Freeze();
        }

        public override void Hide()
        {
            base.Hide();
            FreezeGame.UnFreeze();
        }
    }
}
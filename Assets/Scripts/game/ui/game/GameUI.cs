﻿using TMPro;
using UnityEngine;

namespace game.ui.game
{
    public class GameUI : BaseUI
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI fishText;
        [SerializeField] private TextMeshProUGUI totalFishText;
        [SerializeField] private TextMeshProUGUI timerText;
        [SerializeField] private TextMeshProUGUI mapNameText;

        public void SetScore(int score)
        {
            scoreText.text = $"{score}";
        }

        public void SetFish(int current, int total)
        {
            fishText.text = $"{current}";
            totalFishText.text = $"/{total}";
        }

        public void SetTime(float time)
        {
            timerText.text = $"{time:F1}s";
        }

        public void SetMapName(string mapName)
        {
            mapNameText.text = $"Map: {mapName}";
        }
    }
}
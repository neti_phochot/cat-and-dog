﻿using System.Collections;
using game.world;
using TMPro;
using UnityEngine;

namespace game.ui
{
    public class WorldText : MonoBehaviour, ILocation
    {
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private Canvas canvas;

        public void ShowText(string value, float duration, Camera cam, Color color)
        {
            canvas.worldCamera = cam;
            text.text = value;
            text.color = color;
            transform.rotation = Quaternion.LookRotation(cam.transform.forward);
            StartCoroutine(TextDelay(duration));
        }

        private IEnumerator TextDelay(float duration)
        {
            yield return new WaitForSecondsRealtime(duration);
            DeleteWorldText();
        }

        public Vector3 GetLocation()
        {
            return transform.position;
        }

        public void SetLocation(Vector3 location)
        {
            transform.position = location;
        }

        private void DeleteWorldText()
        {
            Destroy(gameObject);
        }
    }
}
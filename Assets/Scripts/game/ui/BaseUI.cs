﻿using UnityEngine;

namespace game.ui
{
    public abstract class BaseUI : MonoBehaviour, IVisible
    {
        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
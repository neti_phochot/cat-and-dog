﻿using UnityEngine;

namespace game.ui
{
    public class WorldTextManager : MonoBehaviour
    {
        public static WorldTextManager Instance;

        [SerializeField] private Camera cam;
        [SerializeField] private WorldText worldTextPrefab;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
        }

        public void SpawnWorldText(string text, float duration, Vector3 location, Color color)
        {
            WorldText worldText = Instantiate(worldTextPrefab);
            worldText.SetLocation(location);
            worldText.ShowText(text, duration, cam, color);
        }
    }
}
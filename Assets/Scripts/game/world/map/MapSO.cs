﻿using UnityEngine;

namespace game.world.map
{
    [CreateAssetMenu(fileName = "new map data", menuName = "Map Data", order = 0)]
    public class MapSO : ScriptableObject
    {
        public World.WorldData selectedWorldData;
    }
}
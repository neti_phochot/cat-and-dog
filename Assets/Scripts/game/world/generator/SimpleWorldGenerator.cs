﻿using core;
using core.node;
using UnityEngine;

namespace game.world.generator
{
    public class SimpleWorldGenerator : WorldGenerator
    {
        private const string OBSTACLE_COLOR_DATA = "000000";

        [Header("Blocks")] [SerializeField] private Block defaultBlockPrefab;
        [SerializeField] private Block defaultObstaclePrefab;

        public override Block[,] GenerateWorld(Grid<PathNode> grid, World.WorldData worldData)
        {
            Block[,] blocks = new Block[grid.Width, grid.Height];
            for (int x = 0; x < grid.Width; x++)
            {
                for (int y = 0; y < grid.Height; y++)
                {
                    PathNode pathNode = grid.GridObjects[x, y];
                    pathNode.isWalkable = !worldData.GetData(x, y).Equals(OBSTACLE_COLOR_DATA);
                    Block prefab = pathNode.isWalkable ? defaultBlockPrefab : defaultObstaclePrefab;
                    Block block = Instantiate(prefab, transform);
                    block.InitBlock(pathNode);
                    blocks[x, y] = block;
                }
            }

            return blocks;
        }
    }
}
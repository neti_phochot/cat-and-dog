﻿using core;
using core.node;
using UnityEngine;

namespace game.world.generator
{
    public abstract class WorldGenerator : MonoBehaviour
    {
        public abstract Block[,] GenerateWorld(Grid<PathNode> grid, World.WorldData worldData);
    }
}
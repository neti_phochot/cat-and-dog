﻿using System.Collections.Generic;
using core.node;
using game.entity;
using game.entity.fish;
using UnityEngine;

namespace game.world.generator
{
    public abstract class EntityGenerator : MonoBehaviour
    {
        public abstract List<Entity> GenerateEntity(Block[,] blocks, World.WorldData worldData);
    }
}
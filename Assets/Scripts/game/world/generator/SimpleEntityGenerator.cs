﻿using System.Collections.Generic;
using core.node;
using game.entity;
using game.entity.fish;
using UnityEngine;

namespace game.world.generator
{
    public class SimpleEntityGenerator : EntityGenerator
    {
        //KITTEN
        private const string KITTEN_COLOR_DATA = "0000FF";

        //DOG
        private const string RED_COLOR_DATA = "FF0000";
        private const string PINK_COLOR_DATA = "FF00FF";
        private const string BLUE_COLOR_DATA = "00FFED";
        private const string ORANGE_COLOR_DATA = "FFEB04";

        //FISH
        private const string BIG_FISH_COLOR_DATA = "32FF00";
        private const string SMALL_FISH_COLOR_DATA = "808080";

        [Header("PLAYER")] [SerializeField] private Kitten kitten;
        [Header("ENEMY")] [SerializeField] private Dog red;
        [SerializeField] private Dog pinky;
        [SerializeField] private Dog blue;
        [SerializeField] private Dog orange;

        [Header("DOG HOUSE")] [SerializeField] private GameObject dogHouse;

        [Header("ITEM PREFAB")] [SerializeField]
        private BigFish bigFish;

        [SerializeField] private SmallFish smallFish;

        public override List<Entity> GenerateEntity(Block[,] blocks, World.WorldData worldData)
        {
            List<Entity> entities = new List<Entity> { red, pinky, blue, orange };

            for (int i = 0; i < blocks.GetLength(0); i++)
            {
                for (int j = 0; j < blocks.GetLength(1); j++)
                {
                    Block block = blocks[i, j];
                    PathNode pathNode = block.PathNode;
                    if (!pathNode.isWalkable) continue;
                    Entity en = SetEntity(block, worldData.GetData(pathNode.x, pathNode.y));
                    if (!en) continue;
                    entities.Add(en);
                }
            }

            return entities;
        }

        private void SetDogLocation(Dog dog, Block block)
        {
            dog.SetLocation(block.GetLocation());
            dog.GetAI().SpawnPoint = block.GetLocation();
            Instantiate(dogHouse, block.GetLocation(), Quaternion.identity);
        }

        private Entity SetEntity(Block block, string data)
        {
            switch (data)
            {
                //Kitten
                case KITTEN_COLOR_DATA:
                    kitten.SetLocation(block.GetLocation());
                    break;
                //Dog
                case RED_COLOR_DATA:
                    SetDogLocation(red, block);
                    break;
                case PINK_COLOR_DATA:
                    SetDogLocation(pinky, block);
                    break;
                case BLUE_COLOR_DATA:
                    SetDogLocation(blue, block);
                    break;
                case ORANGE_COLOR_DATA:
                    SetDogLocation(orange, block);
                    break;
                //Fish
                case BIG_FISH_COLOR_DATA:
                    return Instantiate(bigFish, block.GetLocation(), Quaternion.identity);
                case SMALL_FISH_COLOR_DATA:
                    return Instantiate(smallFish, block.GetLocation(), Quaternion.identity);
            }

            return default;
        }
    }
}
﻿using UnityEngine;

namespace game.world
{
    public interface IWorld
    {
        Block GetBlockAt(Vector3 location, Vector3 direction);
    }
}
﻿using UnityEngine;

namespace game.world
{
    public class WorldManager : MonoBehaviour
    {
        public static WorldManager Instance;

        [SerializeField] private World world;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
        }

        public World World => world;
    }
}
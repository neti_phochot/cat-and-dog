﻿using System;
using System.Collections.Generic;
using core;
using core.node;
using game.entity;
using game.world.generator;
using game.world.map;
using UnityEngine;
using utils;

namespace game.world
{
    public class World : MonoBehaviour, IWorld
    {
        [Serializable]
        public struct WorldData
        {
            public string worldName;
            public int worldSize;
            public Texture2D worldRawData;

            public string GetData(int x, int y)
            {
                return worldRawData.GetPixel(x, y).ToHexString();
            }
        }

        [Header("World Settings")] [SerializeField]
        private MapSO mapSo;

        [SerializeField] private WorldData defaultWorldData;

        [Space] [SerializeField] private WorldGenerator worldGenerator;
        [SerializeField] private EntityGenerator entityGenerator;
        public WorldData WorldMapData { get; private set; }
        public Grid<PathNode> Grid { get; private set; }
        public AStar PathFinding { get; private set; }
        public Block[,] Blocks { get; private set; }
        public int GetWorldBoundX => Blocks.GetLength(0) * Grid.CellSize;
        public int GetWorldBoundZ => Blocks.GetLength(1) * Grid.CellSize;
        public List<Entity> Entities { get; private set; }

        private void Awake()
        {
            //Init world data
            WorldMapData = mapSo.selectedWorldData.worldRawData == null ? defaultWorldData : mapSo.selectedWorldData;

            //Generate world
            Grid = new Grid<PathNode>(defaultWorldData.worldSize, defaultWorldData.worldSize, 1, CreatePathNode);
            Blocks = worldGenerator.GenerateWorld(Grid, WorldMapData);

            //Spawn Entities
            Entities = entityGenerator.GenerateEntity(Blocks, WorldMapData);
            foreach (var entity in Entities)
            {
                entity.OnEntityDestroyed += en => Entities.Remove(en);
            }

            //Init Pathfinding
            PathFinding = new AStar(Grid);
        }

        private PathNode CreatePathNode(Grid<PathNode> grid, int x, int y)
        {
            return new PathNode(grid, x, y, true);
        }

        public Block GetBlockAt(Vector3 location, Vector3 direction = new Vector3())
        {
            if (direction != Vector3.zero)
            {
                location += direction * Grid.CellSize;
            }

            if (location.x >= 0 && location.x < Blocks.GetLength(0) &&
                location.z >= 0 && location.z < Blocks.GetLength(1))
            {
                int x = Mathf.FloorToInt(location.x) / Grid.CellSize;
                int z = Mathf.FloorToInt(location.z) / Grid.CellSize;
                return Blocks[x, z];
            }

            return default;
        }

        public List<Block> GetWalkAbleBlocks()
        {
            List<Block> blocks = new List<Block>();
            foreach (var block in Blocks)
            {
                if (!block.PathNode.isWalkable) continue;
                blocks.Add(block);
            }

            return blocks;
        }
    }
}
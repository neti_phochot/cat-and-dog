﻿using System;
using core.node;
using UnityEngine;

namespace game.world
{
    [SelectionBase]
    public class Block : MonoBehaviour, ILocation
    {
        public event Action<Block> OnBlockClicked;
        public PathNode PathNode { get; private set; }

        public void InitBlock(PathNode pathNode)
        {
            PathNode = pathNode;
            transform.position = pathNode.ToWorldPosition();
            name = ToString();
        }

        public Vector3 GetLocation() => transform.position;

        public void SetLocation(Vector3 location)
        {
            transform.position = location;
        }


        #region Block Events

        private void OnMouseDown()
        {
            OnBlockClicked?.Invoke(this);
        }

        #endregion

        public override string ToString()
        {
            return $"x:{PathNode.x}, z:{PathNode.y}";
        }
    }
}
﻿using UnityEngine;

namespace game.world
{
    public interface ILocation
    {
        Vector3 GetLocation();
        void SetLocation(Vector3 location);
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using game.world;
using UnityEngine;
using utils;

namespace game.level
{
    public class WorldDataLoader : MonoBehaviour
    {
        [SerializeField] private string mapFolderName;
        [SerializeField] private int limitWidth = 20;
        [SerializeField] private int limitHeight = 21;

        public List<World.WorldData> LoadWorldData()
        {
            List<World.WorldData> worldDatas = new List<World.WorldData>();

            string path = UserPath.GetFolderPath(mapFolderName);

            Debug.Log($"LOAD  PATH {path}");
            //No custom map folder
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                Debug.Log("Maps folder not found!");
                return worldDatas;
            }

            List<PixelTool.TexData> textures = PixelTool.GetTextures(path);
            foreach (var data in textures)
            {
                Texture2D tex = data.Texture2D;
                if (tex.width != limitWidth || tex.height != limitHeight)
                {
                    Debug.Log($"Invalid map file size: Width:{tex.width} Height:{tex.height}");
                    continue;
                }

                worldDatas.Add(new World.WorldData
                    {
                        worldName = Path.GetFileNameWithoutExtension(data.Path),
                        worldSize = tex.width,
                        worldRawData = tex,
                    }
                );
            }

            return worldDatas;
        }
    }
}
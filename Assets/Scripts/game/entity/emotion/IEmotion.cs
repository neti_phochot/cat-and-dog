﻿namespace game.entity.emotion
{
    public interface IEmotion
    {
        void SetEmotion(EmotionType emotionType);
        EmotionType GetEmotion();
    }
}
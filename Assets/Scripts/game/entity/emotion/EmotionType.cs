﻿namespace game.entity.emotion
{
    public enum EmotionType
    {
        NORMAL,
        HURT,
        FRIGHTENED,
    }
}
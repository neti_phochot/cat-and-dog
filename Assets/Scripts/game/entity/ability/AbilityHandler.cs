﻿using System;
using System.Collections;
using System.Collections.Generic;
using game.movement;
using UnityEngine;

namespace game.entity.ability
{
    public class AbilityHandler : MonoBehaviour
    {
        public event Action<AbilityType, float> OnAbilityActivated;
        public event Action<AbilityType, float> OnAbilityDeActivated;

        [Header("SPEED")] [SerializeField] private float speedMultiplier = 1.2f;

        private Dictionary<AbilityType, Coroutine> _abilityTask;
        private LivingEntity _livingEntity;

        private void Awake()
        {
            _abilityTask = new Dictionary<AbilityType, Coroutine>();
        }

        public void InitAbilityHandler(LivingEntity livingEntity)
        {
            _livingEntity = livingEntity;
        }

        public void SetAbility(AbilityType abilityType, float duration)
        {
            if (_abilityTask.ContainsKey(abilityType))
            {
                StopCoroutine(_abilityTask[abilityType]);
            }

            Action action = () => { };

            switch (abilityType)
            {
                case AbilityType.SPEED:
                    IMovement movement = _livingEntity.Movement;
                    movement.ResetSpeed();
                    movement.SetMoveSpeed(movement.GetMoveSpeed() * speedMultiplier);
                    action = () => { movement.ResetSpeed(); };
                    break;
                case AbilityType.INVINCIBLE:
                    _livingEntity.Invincible = true;
                    action = () => { _livingEntity.Invincible = false; };
                    break;
            }

            _abilityTask[abilityType] = StartCoroutine(AbilityTask(abilityType, duration, action));
        }

        private IEnumerator AbilityTask(AbilityType abilityType, float duration, Action action)
        {
            OnAbilityActivated?.Invoke(abilityType, duration);
            yield return new WaitForSecondsRealtime(duration);
            action?.Invoke();
            OnAbilityDeActivated?.Invoke(abilityType, duration);
        }
    }
}
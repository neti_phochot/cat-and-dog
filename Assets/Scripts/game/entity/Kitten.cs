﻿using UnityEngine;

namespace game.entity
{
    public class Kitten : LivingEntity
    {
        [Header("KITTEN TYPE")] [SerializeField]
        private Transform normalKitten;

        [SerializeField] private Transform angryKitten;

        public enum KittenType
        {
            NORMAL,
            ANGRY,
        }

        private void OnTriggerEnter(Collider other)
        {
            //Eat Fish
            if (!other.TryGetComponent<IConsumable>(out var consumable)) return;
            consumable.Eat(this);
        }

        public void SetKittenType(KittenType kittenType)
        {
            normalKitten.gameObject.SetActive(kittenType == KittenType.NORMAL);
            angryKitten.gameObject.SetActive(kittenType == KittenType.ANGRY);
        }
    }
}
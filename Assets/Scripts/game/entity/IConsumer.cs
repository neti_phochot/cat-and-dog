﻿using System;

namespace game.entity
{
    public interface IConsumer
    {
        event Action<IConsumable> OnAte;
        void Ate(IConsumable consumable);
        LivingEntity GetConsumer();
    }
}
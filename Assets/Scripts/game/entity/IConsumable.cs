﻿using System;

namespace game.entity
{
    public interface IConsumable
    {
        event Action<IConsumer> OnEaten;
        void Eat(IConsumer consumer);
    }
}
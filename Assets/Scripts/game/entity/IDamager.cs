﻿namespace game.entity
{
    public interface IDamager
    {
        LivingEntity GetDamager();
    }
}
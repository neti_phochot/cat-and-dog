﻿using UnityEngine;

namespace game.entity.fish
{
    public class SmallFish : BaseFish
    {
        public override void Eat(IConsumer consumer)
        {
            base.Eat(consumer);
            Debug.Log("Ate Small fish!");
        }
    }
}
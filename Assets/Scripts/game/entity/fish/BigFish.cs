﻿using UnityEngine;

namespace game.entity.fish
{
    public class BigFish : BaseFish
    {
        public override void Eat(IConsumer consumer)
        {
            base.Eat(consumer);
            Debug.Log("Power up fish!");
        }
    }
}
﻿using System;

namespace game.entity.fish
{
    public abstract class BaseFish : Entity, IConsumable
    {
        public event Action<IConsumer> OnEaten;

        public virtual void Eat(IConsumer consumer)
        {
            consumer.Ate(this);
            OnEaten?.Invoke(consumer);
            Destroy(gameObject);
        }
    }
}
﻿using System;
using game.entity.ability;
using game.movement;
using UnityEngine;

namespace game.entity
{
    public abstract class LivingEntity : Entity, IConsumer, IDamageable, IDamager
    {
        public AbilityHandler Ability { get; private set; }
        public bool Invincible { get; set; } = false;
        public event Action<IConsumable> OnAte;
        public event Action<LivingEntity, IDamager> OnDamage;
        public IMovement Movement { get; private set; }

        protected override void Awake()
        {
            base.Awake();

            //Movement
            Movement = GetComponent<BaseMovement>();
            Debug.Log($"[{name}] Initial movement component.");
            Debug.Assert(Movement != null, $"[{name}] No movement component found!");

            //Ability
            Ability = GetComponent<AbilityHandler>();
            Ability.InitAbilityHandler(this);
            Debug.Log($"[{name}] Initial {nameof(AbilityHandler)} component.");
            Debug.Assert(Ability != null, $"[{name}] No {nameof(AbilityHandler)} component found!");
        }

        public virtual void Ate(IConsumable consumable)
        {
            OnAte?.Invoke(consumable);
        }

        public LivingEntity GetConsumer()
        {
            return this;
        }

        public void Damage(IDamager damager)
        {
            OnDamage?.Invoke(this, damager);
        }

        public LivingEntity GetDamager()
        {
            return this;
        }


        public override Vector3 GetLocation()
        {
            return Movement.GetRealLocation();
        }
    }
}
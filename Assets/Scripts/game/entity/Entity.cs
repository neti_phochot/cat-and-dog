﻿using System;
using game.world;
using UnityEngine;

namespace game.entity
{
    [SelectionBase]
    public abstract class Entity : MonoBehaviour, ILocation
    {
        [SerializeField] private Transform modelGroup;
        public event Action<Entity> OnEntityDestroyed;

        private bool _activeCollider;

        public bool Collider
        {
            get => _activeCollider;
            set
            {
                _activeCollider = value;
                _collider.enabled = value;
            }
        }

        public bool Visible
        {
            get => modelGroup.gameObject.activeSelf;
            set
            {
                modelGroup.gameObject.SetActive(value);
                Collider = value;
            }
        }

        private Collider _collider;

        protected virtual void Awake()
        {
            _collider = GetComponent<Collider>();
            Debug.Assert(_collider != null, $"[{name}] Don't have collider component!");
        }

        public virtual Vector3 GetLocation() => transform.position;

        public void SetLocation(Vector3 location)
        {
            transform.position = location;
        }

        public Vector3 GetLookDirection() => transform.forward;

        public override string ToString()
        {
            return name;
        }

        private void OnDestroy()
        {
            OnEntityDestroyed?.Invoke(this);
        }
    }
}
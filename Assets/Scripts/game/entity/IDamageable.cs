﻿using System;

namespace game.entity
{
    public interface IDamageable
    {
        event Action<LivingEntity, IDamager> OnDamage;
        void Damage(IDamager damager);

    }
}
﻿using game.ai;
using UnityEngine;

namespace game.entity
{
    public abstract class Creature : LivingEntity, IAI
    {
        private AI _ai;


        protected override void Awake()
        {
            base.Awake();

            _ai = GetComponent<AI>();
            Debug.Assert(_ai != null, $"[{name}] No AI component found!");
        }

        #region AI

        public bool HasAI() => _ai;
        public AI GetAI() => _ai;

        public void SetAI(AI ai)
        {
            _ai = ai;
        }

        #endregion
    }
}
﻿using game.ai;
using game.entity.emotion;
using UnityEngine;

namespace game.entity
{
    public class Dog : Creature, IEmotion
    {
        [Header("EMOTION")] [SerializeField] private Transform normalEmotion;
        [SerializeField] private Transform hurtEmotion;
        [SerializeField] private Transform frightenedEmotion;

        private DogAI _dogAI;
        private EmotionType _emotionType;

        public new DogAI GetAI()
        {
            if (!_dogAI)
            {
                _dogAI = GetComponent<DogAI>();
            }

            return _dogAI;
        }

        public void SetEmotion(EmotionType emotionType)
        {
            _emotionType = emotionType;

            normalEmotion.gameObject.SetActive(
                emotionType == EmotionType.NORMAL ||
                emotionType == EmotionType.FRIGHTENED);

            hurtEmotion.gameObject.SetActive(emotionType == EmotionType.HURT);
            frightenedEmotion.gameObject.SetActive(emotionType == EmotionType.FRIGHTENED);
        }

        public EmotionType GetEmotion() => _emotionType;

        private void OnCollisionEnter(Collision collision)
        {
            if (!collision.collider.TryGetComponent<IDamageable>(out var damageable)) return;
            if (!collision.collider.TryGetComponent<IDamager>(out var damager)) return;
            if (damager.GetDamager().Invincible)
            {
                //Damage me
                Damage(damager);
            }
            else
            {
                //Damage player
                damageable.Damage(this);
            }
        }
    }
}
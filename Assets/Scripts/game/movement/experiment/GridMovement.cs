﻿using UnityEngine;

namespace game.movement.experiment
{
    public class GridMovement : BaseMovement
    {
        [Header("POINTER")] [SerializeField] private float pointerSpeedMultiplier = 1.5f;
        [SerializeField] private float keepDistanceFromTarget = 1f;

        private GridPointer _gridPointer;

        private void Start()
        {
            _gridPointer = new GameObject($"{name}").AddComponent<GridPointer>();
            _gridPointer.Init(this);
            _gridPointer.pointerSpeedMultiplier = pointerSpeedMultiplier;
            _gridPointer.keepDistanceFromTarget = keepDistanceFromTarget;
        }

        public override void MoveTo(Vector3 position)
        {
            //Update Pointer
            _gridPointer.PointTo(transform.position, position);

            //Check if arrived goal 
            if (Vector3.Distance(_gridPointer.transform.position, transform.position) < 0.01) return;

            //Follower pointer
            Vector3 targetPosition = _gridPointer.transform.position;
            transform.rotation = Quaternion.LookRotation(targetPosition - transform.position);
            transform.Translate(Vector3.forward * GetMoveSpeed() * Time.deltaTime);
        }

        public override Vector3 GetRealLocation()
        {
            return _gridPointer.transform.position;
        }
    }
}
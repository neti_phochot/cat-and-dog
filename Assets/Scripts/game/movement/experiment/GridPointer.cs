﻿using UnityEngine;
using utils;

namespace game.movement.experiment
{
    public class GridPointer : MonoBehaviour
    {
        public float pointerSpeedMultiplier = 1.5f;
        public float keepDistanceFromTarget = 1f;

        private BaseMovement _movement;
        private Transform _pointer;

        public void Init(BaseMovement movement)
        {
            transform.parent = null;

            _movement = movement;

            _pointer = new GameObject($"{name}'s Pointer").transform;
            name = $"{name}'s Snap Pointer";

            transform.position = movement.transform.position;
            _pointer.position = movement.transform.position;
        }

        public void PointTo(Vector3 from, Vector3 to)
        {
            Vector3 moveDirection = (to - from).normalized;

            //Check if pointer is too far from follower 
            if (Vector3.Distance(transform.position, _movement.transform.position) > keepDistanceFromTarget) return;

            //Get target block position from direction
            Vector3 targetPosition = (transform.position + moveDirection).ToSnapVector3();

            //Move pointer
            float pointerMoveSpeed = _movement.GetMoveSpeed() * pointerSpeedMultiplier;

            //Move toward target and ignored rotation
            var newPointerPosition = Vector3.MoveTowards(
                _pointer.position, targetPosition,
                pointerMoveSpeed * Time.deltaTime);

            _pointer.position = newPointerPosition;

            Vector3 currentPosition = newPointerPosition.ToSnapVector3();
            if (currentPosition == targetPosition)
            {
                //Snap to block position
                transform.position = targetPosition;
            }
        }

        #region DEBUG

        private void OnDrawGizmos()
        {
            if (!Application.isPlaying) return;
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position, 0.5f);
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(_pointer.position, 0.5f);
            Gizmos.color = Color.white;
            Gizmos.DrawLine(_movement.transform.position, transform.position);
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, _pointer.position);
        }

        #endregion
    }
}
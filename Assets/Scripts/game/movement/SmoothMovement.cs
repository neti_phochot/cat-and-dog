﻿using UnityEngine;

namespace game.movement
{
    public class SmoothMovement : BaseMovement
    {
        [SerializeField] private float turnSpeed = 5f;

        public override void MoveTo(Vector3 position)
        {
            //Smooth Linear Interpolation
            Quaternion lookRotation = Quaternion.LookRotation(position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, turnSpeed * Time.deltaTime);
            transform.Translate(Vector3.forward * GetMoveSpeed() * Time.deltaTime);
        }
    }
}
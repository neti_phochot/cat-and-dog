﻿using UnityEngine;

namespace game.movement
{
    public interface IMovement
    {
        float GetMoveSpeed();
        void SetMoveSpeed(float value);
        void MoveTo(Vector3 position);
        void ResetSpeed();
        Vector3 GetRealLocation();
    }
}
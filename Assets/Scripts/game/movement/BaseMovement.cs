﻿using System;
using UnityEngine;

namespace game.movement
{
    public abstract class BaseMovement : MonoBehaviour, IMovement
    {
        [Header("MOVEMENT")] [SerializeField] private float moveSpeed;

        private float _initMoveSpeed;

        protected virtual void Awake()
        {
            _initMoveSpeed = moveSpeed;
        }

        public float GetMoveSpeed()
        {
            return moveSpeed;
        }

        public void SetMoveSpeed(float value)
        {
            moveSpeed = value;
        }

        public void ResetSpeed()
        {
            moveSpeed = _initMoveSpeed;
        }

        public abstract void MoveTo(Vector3 position);

        public virtual Vector3 GetRealLocation()
        {
            return transform.position;
        }
    }
}
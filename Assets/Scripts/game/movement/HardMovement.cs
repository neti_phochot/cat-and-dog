﻿using UnityEngine;

namespace game.movement
{
    public class HardMovement : BaseMovement
    {
        public override void MoveTo(Vector3 position)
        {
            //Hard look
            transform.rotation = Quaternion.LookRotation(position - transform.position);
            transform.Translate(Vector3.forward * GetMoveSpeed() * Time.deltaTime);
        }
    }
}
﻿using game.world;
using UnityEngine;

namespace game.movement.pointer
{
    public class MovePointer : MonoBehaviour
    {
        public float pointerSpeedMultiplier = 1.5f;
        public float keepDistanceFromTarget = 1f;

        private BaseMovement _movement;
        private World _world;
        private Transform _testMovePointer;

        public void Init(BaseMovement movement)
        {
            transform.parent = null;

            _movement = movement;
            _world = WorldManager.Instance.World;

            _testMovePointer = new GameObject($"{name}'s Pointer").transform;
            name = $"{name}'s Snap Pointer";

            transform.position = movement.transform.position;
            _testMovePointer.position = movement.transform.position;
        }

        public void MoveToDirection(Vector3 direction)
        {
            //Check if pointer is too far from follower 
            if (Vector3.Distance(transform.position, _movement.transform.position) > keepDistanceFromTarget) return;

            //Test move, Get target block position from direction
            Block targetBlock = _world.GetBlockAt(transform.position + direction);
            if (!targetBlock || !targetBlock.PathNode.isWalkable) return;

            //Move toward target and ignored rotation
            float testMoveSpeed = _movement.GetMoveSpeed() * pointerSpeedMultiplier;
            var testMovePosition = Vector3.MoveTowards(
                _testMovePointer.position, targetBlock.GetLocation(),
                testMoveSpeed * Time.deltaTime);

            //Move pointer to test move position
            _testMovePointer.position = testMovePosition;

            //Check if outside grid
            Block currentBlock = _world.GetBlockAt(testMovePosition);
            if (!currentBlock) return;

            if (currentBlock == targetBlock)
            {
                //Snap to block position
                transform.position = targetBlock.GetLocation();
            }
        }

        #region DEBUG

        private void OnDrawGizmos()
        {
            if (!Application.isPlaying) return;
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position, 0.5f);
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(_testMovePointer.position, 0.5f);
            Gizmos.color = Color.white;
            Gizmos.DrawLine(_movement.transform.position, transform.position);
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, _testMovePointer.position);
        }

        #endregion
    }
}
﻿using game.movement.pointer;
using UnityEngine;

namespace game.movement
{
    public class PointerMovement : BaseMovement
    {
        [Header("POINTER")] [SerializeField] private float pointerSpeedMultiplier = 1.5f;
        [SerializeField] private float keepDistanceFromTarget = 1f;

        private MovePointer _movePointer;

        private void Start()
        {
            _movePointer = new GameObject($"{name}").AddComponent<MovePointer>();
            _movePointer.Init(this);
            _movePointer.pointerSpeedMultiplier = pointerSpeedMultiplier;
            _movePointer.keepDistanceFromTarget = keepDistanceFromTarget;
        }

        public override void MoveTo(Vector3 position)
        {
            //Update Pointer
            Vector3 moveDirection = (position - transform.position).normalized;
            _movePointer.MoveToDirection(moveDirection);

            //Check if reached goal 
            if (Vector3.Distance(transform.position, _movePointer.transform.position) < 0.01f) return;

            //Follower pointer
            Vector3 targetPosition = _movePointer.transform.position;
            transform.rotation = Quaternion.LookRotation(targetPosition - transform.position);
            transform.Translate(Vector3.forward * GetMoveSpeed() * Time.deltaTime);
        }

        public override Vector3 GetRealLocation()
        {
            return _movePointer.transform.position;
        }
    }
}
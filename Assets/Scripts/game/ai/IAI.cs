﻿namespace game.ai
{
    public interface IAI
    {
        bool HasAI();
        AI GetAI();
        void SetAI(AI ai);
    }
}
﻿using game.ai.state;
using game.entity;
using game.entity.ability;
using UnityEngine;

namespace game.ai
{
    public abstract class DogAI : AI
    {
        public enum DogStateType
        {
            CHASE,
            FRIGHTENED,
            RECOVER,
        }

        public DogStateType DogState { get; private set; }
        public Vector3 SpawnPoint { get; set; }

        protected Kitten Kitten;
        protected Dog Dog;

        protected override void Awake()
        {
            base.Awake();
            Kitten = FindObjectOfType<Kitten>();
            Dog = GetComponent<Dog>();

            Debug.Assert(Kitten != null, "Can't find kitten in scene.");
            Debug.Assert(Dog != null, "Can't find dog component.");

            SpawnPoint = transform.position;
        }

        protected virtual void Start()
        {
            AIState.SetState(GetInitialState());

            Kitten.Ability.OnAbilityActivated += OnAbilityActivatedEvent;
            Kitten.Ability.OnAbilityDeActivated += OnAbilityDeActivatedEvent;
            Dog.OnDamage += OnDamageEvent;
            Kitten.OnDamage += OnKittenDamageEvent;
        }

        private void OnDestroy()
        {
            Kitten.Ability.OnAbilityActivated -= OnAbilityActivatedEvent;
            Kitten.Ability.OnAbilityDeActivated -= OnAbilityDeActivatedEvent;
            Dog.OnDamage -= OnDamageEvent;
            Kitten.OnDamage -= OnKittenDamageEvent;
        }

        public abstract State GetInitialState();
        public abstract State GetState(DogStateType dogStateType);

        public void SetState(DogStateType dogStateType)
        {
            DogState = dogStateType;
            AIState.SetState(GetState(dogStateType));
        }

        #region GLOBAL AI BEHAVIOR

        private void OnAbilityActivatedEvent(AbilityType abilityType, float duration)
        {
            if (DogState == DogStateType.RECOVER) return;
            if (abilityType != AbilityType.INVINCIBLE) return;
            Dog.GetAI().SetState(DogStateType.FRIGHTENED);
        }

        private void OnAbilityDeActivatedEvent(AbilityType abilityType, float duration)
        {
            if (DogState == DogStateType.RECOVER) return;
            if (abilityType != AbilityType.INVINCIBLE) return;
            Dog.GetAI().SetState(DogStateType.CHASE);
        }

        private void OnDamageEvent(LivingEntity dog, IDamager damager)
        {
            //Dog hurt
            if (DogState == DogStateType.RECOVER) return;
            Dog.GetAI().SetState(DogStateType.RECOVER);
        }

        private void OnKittenDamageEvent(LivingEntity livingEntity, IDamager damager)
        {
            //Game over!
            Dog.GetAI().AIState.SetState(new EmptyState());
        }

        #endregion
    }
}
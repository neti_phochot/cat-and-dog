﻿using System.Collections.Generic;
using core;
using core.node;
using game.entity;
using game.world;
using UnityEngine;
using utils;

namespace game.ai.state.dog
{
    public abstract class BaseDogState : State
    {
        private const float DEFAULT_AI_THINKING_INTERVAL = 0.5f;

        protected Dog Dog { get; }
        protected Kitten Kitten { get; }
        protected GameTimer AITimer { get; }
        protected Color DebugTargetColor = Color.red;
        private List<PathNode> _foundPaths = new List<PathNode>();
        private int _pathIndex;

        protected BaseDogState(Dog dog, Kitten kitten)
        {
            Dog = dog;
            Kitten = kitten;

            //Initial AI Timer
            AITimer = new GameTimer(DEFAULT_AI_THINKING_INTERVAL);
            AITimer.OnGameTimerTick += OnAITimerTickEvent;
        }

        protected override void Enter()
        {
            base.Enter();
            AITimer.StartTimer();
        }

        protected override void Update()
        {
            base.Update();
            AITimer.UpdateTimer();

            FollowPath();
            DrawDebugFoundPath();
            DrawDebugTargetPath();
        }

        protected virtual void OnAITimerTickEvent(float tick)
        {
            UpdateDestinationPath();
        }

        private void UpdateDestinationPath()
        {
            Block targetBlock = GetDestinationBlock();
            if (!targetBlock)
            {
                Debug.LogWarning($"[{Dog}] target block is null!");
                return;
            }

            Debug.Log($"[{Dog}] updating Path to {Kitten}");
            World world = WorldManager.Instance.World;
            Block currentBlock = world.GetBlockAt(Dog.GetLocation());
            if (!currentBlock) return;
            PathNode startNode = currentBlock.PathNode;

            _foundPaths = world.PathFinding.FindPath(startNode, targetBlock.PathNode, AStar.DirectionType.FOUR);
            _pathIndex = 0;

            if (_foundPaths.Count == 0)
            {
                Debug.LogWarning($"[{Dog}] can't find path to {Kitten}");
            }
        }

        private void FollowPath()
        {
            if (_foundPaths.Count == 0) return;
            
            //Check if we arrived goal point
            if (_pathIndex > _foundPaths.Count - 1)
            {
                _foundPaths.Clear();
                return;
            }

            Vector3 destination = _foundPaths[_pathIndex].ToWorldPosition();
            if (Vector3.Distance(Dog.GetLocation(), destination) < 1f)
            {
                _pathIndex++;
            }

            Vector3 targetBlockDir = (destination - Dog.GetLocation()).normalized;
            Dog.Movement.MoveTo(Dog.transform.position + targetBlockDir);
        }

        protected abstract Block GetDestinationBlock();

        #region DEBUG

        private void DrawDebugFoundPath()
        {
            if (_foundPaths.Count > 0)
            {
                for (int i = 0; i < _foundPaths.Count; i++)
                {
                    if (i == _foundPaths.Count - 1) continue;
                    PathNode a = _foundPaths[i];
                    PathNode b = _foundPaths[i + 1];
                    Vector3 offset = new Vector3(0, 0.2f);
                    Debug.DrawLine(a.ToWorldPosition() + offset, b.ToWorldPosition() + offset);
                }
            }
        }

        private void DrawDebugTargetPath()
        {
            Block targetBlock = GetDestinationBlock();
            if (targetBlock == null) return;
            World world = WorldManager.Instance.World;
            Block startBlock = world.GetBlockAt(Dog.GetLocation());
            if (startBlock == null) return;
            PathNode startNode = startBlock.PathNode;
            PathNode goalNode = targetBlock.PathNode;
            Vector3 offset = new Vector3(0, 0.2f);
            Debug.DrawLine(startNode.ToWorldPosition() + offset, goalNode.ToWorldPosition() + offset, DebugTargetColor);
        }

        #endregion
    }
}
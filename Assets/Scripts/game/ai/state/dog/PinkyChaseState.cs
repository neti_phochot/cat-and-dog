﻿using game.entity;
using game.world;
using UnityEngine;

namespace game.ai.state.dog
{
    public class PinkyChaseState : BaseDogState
    {
        public PinkyChaseState(Dog dog, Kitten kitten) : base(dog, kitten)
        {
            DebugTargetColor = Color.magenta;
        }

        protected override Block GetDestinationBlock()
        {
            //Pinky: Spot 4 tiles ahead kitten
            float spotAheadLength = 4f;
            Vector3 spotAHeadTarget = Kitten.GetLocation() + Kitten.GetLookDirection() * spotAheadLength;
            return WorldManager.Instance.World.GetBlockAt(spotAHeadTarget);
        }
    }
}
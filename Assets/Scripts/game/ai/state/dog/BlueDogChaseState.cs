﻿using game.entity;
using game.world;
using UnityEngine;

namespace game.ai.state.dog
{
    public class BlueDogChaseState : BaseDogState
    {
        public BlueDogChaseState(Dog dog, Kitten kitten) : base(dog, kitten)
        {
            DebugTargetColor = Color.cyan;
        }

        protected override Block GetDestinationBlock()
        {
            //Blue: Spot 2 tiles in front of kitten and extending the line again at the same lenght from Pinky
            
            foreach (var entity in WorldManager.Instance.World.Entities)
            {
                if (entity.TryGetComponent(out PinkyDogAI pinkyDogAI))
                {
                    Dog pinkyDog = pinkyDogAI.gameObject.GetComponent<Dog>();
                    Vector3 pinkyLocation = pinkyDog.GetLocation();

                    float spotAheadLength = 2f;
                    Vector3 spotAHeadTarget = Kitten.GetLocation() + Kitten.GetLookDirection() * spotAheadLength;

                    Vector3 spotFromPinky = spotAHeadTarget - pinkyLocation;
                    Vector3 extendingSpot = spotFromPinky * 2f;
                    Vector3 blueSpot = pinkyLocation + extendingSpot;
                    Debug.DrawRay(pinkyLocation, extendingSpot, Color.blue);
                    return WorldManager.Instance.World.GetBlockAt(blueSpot);
                }
            }

            Debug.LogWarning($"[{Dog}] Cannot find Pinky in scene!");
            return default;
        }
    }
}
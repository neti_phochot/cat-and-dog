﻿using System.Collections.Generic;
using System.Linq;
using game.entity;
using game.entity.emotion;
using game.movement;
using game.world;
using UnityEngine;

namespace game.ai.state.dog
{
    public class FrightenedState : BaseDogState
    {
        private static List<Block> _awayFromKittenBlocks;
        private static string _processor;

        private const float MIN_DISTANCE_FROM_KITTEN_FACTOR = 0.7f;
        private const float MOVE_SPEED_FACTOR = 1.1f;

        private List<Block> _walkableBlocks = new List<Block>();
        private Block _farthestBlock;
        private float _minDistanceFromKitten;


        public FrightenedState(Dog dog, Kitten kitten) : base(dog, kitten)
        {
            if (string.IsNullOrEmpty(_processor))
            {
                _processor = dog.name;
            }
        }

        protected override void Enter()
        {
            base.Enter();
            _walkableBlocks = WorldManager.Instance.World.GetWalkAbleBlocks();

            //Slow down move speed
            IMovement movement = Dog.Movement;
            movement.ResetSpeed();
            movement.SetMoveSpeed(movement.GetMoveSpeed() * MOVE_SPEED_FACTOR);

            //Calculate max distance away from player
            World world = WorldManager.Instance.World;
            float distAwayFromPlayer = Mathf.Min(world.GetWorldBoundX, world.GetWorldBoundZ);
            _minDistanceFromKitten = distAwayFromPlayer * MIN_DISTANCE_FROM_KITTEN_FACTOR;
            
            Dog.SetEmotion(EmotionType.FRIGHTENED);
        }

        protected override void Exit()
        {
            base.Exit();
            Dog.SetEmotion(EmotionType.NORMAL);
        }

        protected override Block GetDestinationBlock()
        {
            if (!_farthestBlock || Vector3.Distance(Dog.GetLocation(), _farthestBlock.GetLocation()) < 1f)
            {
                _farthestBlock = FindBlockToRunAway();
            }

            return _farthestBlock;
        }

        private Block FindBlockToRunAway()
        {
            if (_processor == Dog.name)
            {
                _awayFromKittenBlocks = (from block in _walkableBlocks
                    let dist = Vector3.Distance(block.GetLocation(), Kitten.GetLocation())
                    where !(dist < _minDistanceFromKitten)
                    select block).ToList();

                Debug.Log($">> {Dog} found {_awayFromKittenBlocks.Count} blocks to run away!");
            }

            return _awayFromKittenBlocks.Count > 0
                ? _awayFromKittenBlocks[Random.Range(0, _awayFromKittenBlocks.Count)]
                : _walkableBlocks[Random.Range(0, _walkableBlocks.Count)];
        }
    }
}
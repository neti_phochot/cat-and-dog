﻿using System.Collections.Generic;
using game.entity;
using game.world;
using UnityEngine;

namespace game.ai.state.dog
{
    public class OrangeChaseState : BaseDogState
    {
        private const float AWAY_DISTANCE_FROM_KITTEN = 3f;

        private List<Block> _walkableBlocks = new List<Block>();
        private Block _randomBlock;

        public OrangeChaseState(Dog dog, Kitten kitten) : base(dog, kitten)
        {
            DebugTargetColor = Color.yellow;
        }

        protected override void Enter()
        {
            base.Enter();
            _walkableBlocks = WorldManager.Instance.World.GetWalkAbleBlocks();
            if (_walkableBlocks.Count == 0)
            {
                Debug.LogWarning($"[{Dog}] Cannot found walkable blocks!");
            }
            else
            {
                Debug.Log($"[{Dog}] Found Walkable Blocks: {_walkableBlocks.Count}");
            }
        }

        protected override Block GetDestinationBlock()
        {
            //Orange: Scatter Away when in kitten radius
            if (_randomBlock == null &&
                Vector3.Distance(Dog.GetLocation(), Kitten.GetLocation()) < AWAY_DISTANCE_FROM_KITTEN)
            {
                //Go to random block
                Debug.Log($"[{Dog}] Try to scatter away from kitten!");
                _randomBlock = _walkableBlocks[Random.Range(0, _walkableBlocks.Count)];
                return _randomBlock;
            }


            if (_randomBlock != null)
            {
                if (Vector3.Distance(Dog.GetLocation(), _randomBlock.GetLocation()) < 1f)
                {
                    _randomBlock = null;
                }
                else
                {
                    return _randomBlock;
                }
            }

            //Go toward to kitten
            return WorldManager.Instance.World.GetBlockAt(Kitten.GetLocation());
        }
    }
}
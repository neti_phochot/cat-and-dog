﻿using game.entity;
using game.entity.ability;
using game.entity.emotion;
using game.fx;
using game.world;
using UnityEngine;
using utils;

namespace game.ai.state.dog
{
    public class DogRecoverState : BaseDogState
    {
        private const float RECOVER_DURATION = 8f;
        private Block _dogHouseBlock;

        public DogRecoverState(Dog dog, Kitten kitten) : base(dog, kitten)
        {
        }

        protected override void Enter()
        {
            base.Enter();
            _dogHouseBlock = WorldManager.Instance.World.GetBlockAt(Dog.GetAI().SpawnPoint);

            Dog.Collider = false;
            Dog.Ability.SetAbility(AbilityType.SPEED, float.MaxValue);
        }

        protected override void Update()
        {
            base.Update();
            CheckIsInDogHouse();

            Dog.SetEmotion(Blink.Instance.IsBlink ? EmotionType.HURT : EmotionType.NORMAL);

            if (TimeSinceStateStarts < RECOVER_DURATION)
                return;
            Dog.GetAI().SetState(DogAI.DogStateType.CHASE);
        }

        private void CheckIsInDogHouse()
        {
            if (!Dog.Visible || Vector3.Distance(Dog.GetLocation(), _dogHouseBlock.GetLocation()) > 1f) return;
            Dog.Visible = false;
            GameFX.Instance.PlayEffect(GameFX.EffectType.SMOKE, Dog.GetLocation());
        }

        protected override void Exit()
        {
            base.Exit();
            Dog.Visible = true;
            Dog.Ability.SetAbility(AbilityType.SPEED, 0);

            Dog.SetEmotion(EmotionType.NORMAL);
            GameFX.Instance.PlayEffect(GameFX.EffectType.SMOKE, Dog.GetLocation());
        }

        protected override Block GetDestinationBlock()
        {
            return _dogHouseBlock;
        }
    }
}
﻿using game.entity;
using game.world;
using UnityEngine;

namespace game.ai.state.dog
{
    public class RedDogChaseState : BaseDogState
    {
        public RedDogChaseState(Dog dog, Kitten kitten) : base(dog, kitten)
        {
            DebugTargetColor = Color.red;
        }

        protected override Block GetDestinationBlock()
        {
            //Red: Go toward to kitten
            return WorldManager.Instance.World.GetBlockAt(Kitten.GetLocation());
        }
    }
}
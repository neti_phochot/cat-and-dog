﻿using UnityEngine;

namespace game.ai.state
{
    public abstract class State
    {
        protected float TimeSinceStateStarts { get; private set; }

        private enum StateType
        {
            ENTER,
            UPDATE,
            EXIT
        }

        private StateType _stateType;

        private State _nextState;
        private State _currentState;

        protected State()
        {
            _currentState = this;
            _stateType = StateType.ENTER;
        }

        public void SetState(State nextState)
        {
            Debug.Log($"Set state to: {nextState.GetType()}");
            _nextState = nextState;
            _stateType = StateType.EXIT;
        }

        protected virtual void Enter()
        {
            Debug.Log($"Entering state: {_currentState.GetType()}");
            _stateType = StateType.UPDATE;
        }

        protected virtual void Update()
        {
            _stateType = StateType.UPDATE;
            TimeSinceStateStarts += Time.deltaTime;
        }

        protected virtual void Exit()
        {
            Debug.Log($"Leaving state: {_currentState.GetType()}");
        }

        public State Process()
        {
            if (_stateType == StateType.ENTER) Enter();
            else if (_stateType == StateType.UPDATE) Update();
            else if (_stateType == StateType.EXIT)
            {
                Exit();
                _currentState = _nextState;
            }

            return _currentState;
        }
    }
}
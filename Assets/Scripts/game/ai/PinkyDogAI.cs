﻿using game.ai.state;
using game.ai.state.dog;

namespace game.ai
{
    public class PinkyDogAI : DogAI
    {
        public override State GetInitialState()
        {
            return GetState(DogStateType.CHASE);
        }

        public override State GetState(DogStateType dogStateType)
        {
            switch (dogStateType)
            {
                case DogStateType.CHASE:
                    return new PinkyChaseState(Dog, Kitten);
                case DogStateType.FRIGHTENED:
                    return new FrightenedState(Dog, Kitten);
                case DogStateType.RECOVER:
                    return new DogRecoverState(Dog, Kitten);
                default:
                    return GetInitialState();
            }
        }
    }
}
﻿using game.ai.state;
using UnityEngine;

namespace game.ai
{
    public abstract class AI : MonoBehaviour
    {
        public State AIState { get; private set; }

        protected virtual void Awake()
        {
            AIState = new EmptyState();
            Debug.Log($"[{name}] Initial AI state: {GetType()}.");
        }

        private void Update()
        {
            AIState = AIState?.Process();
        }
    }
}
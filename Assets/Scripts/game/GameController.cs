﻿using game.entity;
using game.entity.ability;
using game.entity.fish;
using game.fx;
using game.ui;
using game.ui.game;
using game.world;
using game.world.map;
using UnityEngine;
using utils;

namespace game
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private MapSO mapSo;
        [SerializeField] private float freezeDuration = 2f;
        [Header("ABILITY")] [SerializeField] private float powerUpDuration = 5f;
        [Header("SCORE")] [SerializeField] private int bigFishScore = 20;
        [SerializeField] private int smallFishScore = 10;
        [SerializeField] private int hitDogScore = 10;
        [SerializeField] private float streakScoreTime = 4f;
        [Header("DAMAGE")] [SerializeField] private float meowTextTime = 1f;

        [Header("WORLD SCORE TEXT")] [SerializeField]
        private Vector3 scoreTextOffset;

        public bool IsGameOver { get; private set; }
        private int _score;

        private Kitten _kitten;
        private Dog[] _dogs;

        private int _scoreStreak;
        private float _lastScoreStreakTime;

        private int _currentFish;
        private int _totalFish;

        private float _startTime;

        private void Start()
        {
            _kitten = FindObjectOfType<Kitten>();
            _dogs = FindObjectsOfType<Dog>();
            _kitten.OnAte += OnKittenAteEvent;

            _kitten.Ability.OnAbilityActivated += OnAbilityActivatedEvent;
            _kitten.Ability.OnAbilityDeActivated += OnAbilityDeActivatedEvent;
            _kitten.OnDamage += OnKittenDamageEvent;

            foreach (var dog in _dogs)
            {
                dog.OnDamage += OnDogDamageEvent;
            }

            GameUIManager.Instance.SetScore(_score);
            GameUIManager.Instance.SetFish(_currentFish, _totalFish);
            GameUIManager.Instance.SetMapName(mapSo.selectedWorldData.worldName);

            _startTime = Time.time;
        }

        private void Update()
        {
            UpdateTimer();
        }

        private void UpdateTimer()
        {
            if (IsGameOver) return;
            float timer = Time.time - _startTime;
            GameUIManager.Instance.SetGameTimer(timer);
        }

        private void OnDestroy()
        {
            _kitten.OnAte -= OnKittenAteEvent;

            _kitten.Ability.OnAbilityActivated -= OnAbilityActivatedEvent;
            _kitten.Ability.OnAbilityDeActivated -= OnAbilityDeActivatedEvent;
            _kitten.OnDamage -= OnKittenDamageEvent;
            foreach (var dog in _dogs)
            {
                dog.OnDamage -= OnDogDamageEvent;
            }
        }

        private void InitFishCount()
        {
            foreach (var en in WorldManager.Instance.World.Entities)
            {
                if (en is BaseFish)
                {
                    _totalFish++;
                }
            }

            Debug.Log($"Found {_currentFish} fish!");
        }

        private void OnKittenAteEvent(IConsumable consumable)
        {
            if (_totalFish == 0)
            {
                InitFishCount();
            }

            if (consumable is BigFish)
            {
                _score += bigFishScore;
                _currentFish++;

                //Add ability
                _kitten.Ability.SetAbility(AbilityType.SPEED, powerUpDuration);
                _kitten.Ability.SetAbility(AbilityType.INVINCIBLE, powerUpDuration);
            }
            else if (consumable is SmallFish)
            {
                _score += smallFishScore;
                _currentFish++;
            }

            if (_currentFish == _totalFish)
            {
                //You win!
                _kitten.gameObject.SetActive(false);
                GameUIManager.Instance.SetGameResult(true);
                IsGameOver = true;
            }

            GameFX.Instance.PlayEffect(GameFX.EffectType.PICKUP, _kitten.GetLocation());
            GameUIManager.Instance.SetScore(_score);
            GameUIManager.Instance.SetFish(_currentFish, _totalFish);
        }

        private void OnAbilityActivatedEvent(AbilityType abilityType, float duration)
        {
            Debug.Log($"[{_kitten}] active ability: {abilityType}");
        }

        private void OnAbilityDeActivatedEvent(AbilityType abilityType, float duration)
        {
            Debug.Log($"[{_kitten}] de-active ability: {abilityType}");
        }

        private void OnKittenDamageEvent(LivingEntity victim, IDamager damager)
        {
            void EnterAction()
            {
                WorldTextManager.Instance.SpawnWorldText("~Meow ;w;", meowTextTime,
                    victim.GetLocation() + scoreTextOffset, Color.white);
            }

            void ExitAction()
            {
                victim.gameObject.SetActive(false);
                GameUIManager.Instance.SetGameResult(false);
                IsGameOver = true;
            }

            FreezeGame.Instance.FreezeAction(freezeDuration, EnterAction, ExitAction);
        }

        private void OnDogDamageEvent(LivingEntity victim, IDamager damager)
        {
            void EnterAction()
            {
            }

            void ExitAction()
            {
            }

            int score = GetScoreStreak();
            _score += score;

            WorldTextManager.Instance.SpawnWorldText(
                $"+{score}", freezeDuration / 2f,
                victim.GetLocation() + scoreTextOffset, Color.yellow);

            FreezeGame.Instance.FreezeAction(freezeDuration, EnterAction, ExitAction);
            GameUIManager.Instance.SetScore(_score);
        }

        #region SCORE STREATK

        private int GetScoreStreak()
        {
            float lastStreakScore = Time.time - _lastScoreStreakTime;
            if (lastStreakScore > streakScoreTime)
            {
                _lastScoreStreakTime = Time.time;
                _scoreStreak = 0;
                return hitDogScore;
            }
            
            //X2 score
            _lastScoreStreakTime = Time.time;
            if (_scoreStreak == 0)
            {
                _scoreStreak = hitDogScore;
            }
            _scoreStreak *= 2;
            return _scoreStreak ;
        }

        #endregion
    }
}
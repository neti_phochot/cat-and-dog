﻿using System.Linq;
using UnityEngine;

namespace game.fx
{
    public class GameFX : MonoBehaviour
    {
        public enum EffectType
        {
            SMOKE,
            PICKUP,
        }

        public static GameFX Instance;

        [SerializeField] private Transform smokeEffect;
        [SerializeField] private Transform[] pickupEffects;

        private int _pickupIndex;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
        }


        public void PlayEffect(EffectType effectType, Vector3 location)
        {
            switch (effectType)
            {
                case EffectType.SMOKE:
                    smokeEffect.transform.position = location;
                    smokeEffect.gameObject.SetActive(false);
                    smokeEffect.gameObject.SetActive(true);
                    break;
                case EffectType.PICKUP:
                    Transform fx = pickupEffects[_pickupIndex++ % pickupEffects.Length];
                    fx.transform.position = location;
                    fx.gameObject.SetActive(false);
                    fx.gameObject.SetActive(true);
                    break;
            }
        }
    }
}
﻿using game.entity;
using game.entity.ability;
using UnityEngine;
using utils;

namespace game.fx
{
    public class KittenVisual : MonoBehaviour
    {
        private Kitten _kitten;

        private float _maxDuration;
        private float _duration;

        private bool _abilityActive;

        private void Awake()
        {
            _kitten = GetComponent<Kitten>();
        }

        private void Start()
        {
            _kitten.Ability.OnAbilityActivated += OnAbilityActivatedEvent;
            _kitten.Ability.OnAbilityDeActivated += OnAbilityDeActivatedEvent;
        }

        private void OnDestroy()
        {
            _kitten.Ability.OnAbilityActivated -= OnAbilityActivatedEvent;
            _kitten.Ability.OnAbilityDeActivated -= OnAbilityDeActivatedEvent;
        }


        private void Update()
        {
            CheckAbilityTime();
        }

        private void CheckAbilityTime()
        {
            if (!_abilityActive) return;
            _duration -= Time.deltaTime;
            if (_duration < _maxDuration * 0.3f)
            {
                _kitten.SetKittenType(Blink.Instance.IsBlink ? Kitten.KittenType.ANGRY : Kitten.KittenType.NORMAL);
            }
        }

        private void OnAbilityActivatedEvent(AbilityType abilityType, float duration)
        {
            _maxDuration = duration;
            _duration = duration;
            _kitten.SetKittenType(Kitten.KittenType.ANGRY);

            if (_abilityActive) return;
            _abilityActive = true;
            GameFX.Instance.PlayEffect(GameFX.EffectType.SMOKE, _kitten.GetLocation());
        }

        private void OnAbilityDeActivatedEvent(AbilityType abilityType, float duration)
        {
            _abilityActive = false;
            _kitten.SetKittenType(Kitten.KittenType.NORMAL);
            GameFX.Instance.PlayEffect(GameFX.EffectType.SMOKE, _kitten.GetLocation());
        }
    }
}
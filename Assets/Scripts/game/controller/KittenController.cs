﻿using game.movement;
using game.world;
using UnityEngine;

namespace game.controller
{
    public class KittenController : MonoBehaviour
    {
        [SerializeField] private float minMoveDistance = 0.01f;

        private float _horizontal;
        private float _vertical;

        private IMovement _movement;
        private Vector3 _moveDirection;

        private World _world;

        private void Start()
        {
            _movement = GetComponent<BaseMovement>();
            _world = WorldManager.Instance.World;
        }

        private void Update()
        {
            PlayerInput();
        }

        private void SetMoveDirection(Vector3 direction)
        {
            if (!IsValidDirection(direction)) return;
            _moveDirection = direction;
        }

        private void PlayerInput()
        {
            if (Mathf.Abs(_horizontal = Input.GetAxisRaw("Horizontal")) > minMoveDistance)
            {
                SetMoveDirection(_horizontal > 0 ? Vector3.right : -Vector3.right);
            }
            else if (Mathf.Abs(_vertical = Input.GetAxisRaw("Vertical")) > minMoveDistance)
            {
                SetMoveDirection(_vertical > 0 ? Vector3.forward : -Vector3.forward);
            }

            _movement.MoveTo(transform.position + _moveDirection);
        }


        private bool IsValidDirection(Vector3 direction)
        {
            Block targetBlock = _world.GetBlockAt(_movement.GetRealLocation() + direction);
            return targetBlock && targetBlock.PathNode.isWalkable;
        }
    }
}
﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace utils
{
    public static class PixelTool
    {
        public struct TexData
        {
            public string Path;
            public Texture2D Texture2D;
        }

        public static Texture2D GetTexture2DFromFile(string filePath, FilterMode filterMode = FilterMode.Point)
        {
            //https://answers.unity.com/questions/432655/loading-texture-file-from-pngjpg-file-on-disk.html
            var fileData = File.ReadAllBytes(filePath);
            var tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
            tex.filterMode = filterMode;
            return tex;
        }

        public static List<TexData> GetTextures(string directoryPath, FilterMode filterMode = FilterMode.Point)
        {
            List<TexData> result = new List<TexData>();
            foreach (var file in Directory.GetFiles(directoryPath))
            {
                if (!Path.GetExtension(file).ToLower().Equals(".png")) continue;
                result.Add(new TexData
                {
                    Path = file,
                    Texture2D = GetTexture2DFromFile(file, filterMode),
                });
            }

            return result;
        }
    }
}
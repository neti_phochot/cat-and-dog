﻿using System.IO;
using UnityEngine;

namespace utils
{
    public class UserPath
    {
        public static string GetRoot()
        {
            return Path.GetDirectoryName(Application.dataPath);
        }

        public static string GetFolderPath(string folderName)
        {
            return Path.Combine(GetRoot(), folderName);
        }
    }
}
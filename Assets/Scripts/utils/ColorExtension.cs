﻿using UnityEngine;

namespace utils
{
    public static class ColorExtension
    {
        //Source: https://youtu.be/CMGn2giYLc8
        private static int HexToDec(string hex)
        {
            return System.Convert.ToInt32(hex, 16);
        }

        private static string DecToHex(int dec)
        {
            return dec.ToString("X2");
        }

        private static int FloatNormalizeToInt(float value)
        {
            return Mathf.RoundToInt(value * 255f);
        }

        public static Color ToHexColor(this string hex)
        {
            int rDec = HexToDec(hex.Substring(0, 2));
            int gDec = HexToDec(hex.Substring(2, 2));
            int bDec = HexToDec(hex.Substring(4, 2));
            return new Color(rDec, gDec, bDec, 1);
        }

        public static string ToHexString(this Color color)
        {
            string rHex = DecToHex(FloatNormalizeToInt(color.r));
            string gHex = DecToHex(FloatNormalizeToInt(color.g));
            string bHex = DecToHex(FloatNormalizeToInt(color.b));
            return $"{rHex}{gHex}{bHex}";
        }
    }
}
﻿using UnityEngine;

namespace utils
{
    public class FollowTarget : MonoBehaviour
    {
        [SerializeField] private Transform follow;

        private void Awake()
        {
            transform.parent = null;
        }

        private void FixedUpdate()
        {
            transform.position = follow.position;
        }
    }
}
﻿using System;
using UnityEngine;

namespace utils
{
    public class GameTimer
    {
        public Action<float> OnGameTimerTick;
        public float Timer { get; private set; }
        public float Interval { get; private set; }

        private float _currentInterval;
        public bool IsActive { get; set; }

        public GameTimer(float interval)
        {
            Interval = interval;
        }

        public void SetUpdateThreshold(float interval)
        {
            Interval = interval;
        }

        public void StartTimer()
        {
            _currentInterval = Interval;
            Timer = 0f;
            IsActive = true;
        }

        public void UpdateTimer()
        {
            if (!IsActive) return;
            _currentInterval -= Time.deltaTime;
            if (_currentInterval < 0f)
            {
                _currentInterval = Interval;
                Timer += Interval;
                OnGameTimerTick?.Invoke(Timer);
            }
        }
    }
}
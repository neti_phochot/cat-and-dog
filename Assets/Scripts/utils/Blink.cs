﻿using UnityEngine;

namespace utils
{
    public class Blink : MonoBehaviour
    {
        private static Blink _instance;
        public bool IsBlink { get; private set; }

        private const float SPEED = 15f;
        private float _startTime;

        public static Blink Instance
        {
            get
            {
                if (!_instance)
                {
                    _instance = new GameObject($"{nameof(Blink)}").AddComponent<Blink>();
                }

                return _instance;
            }
        }

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                return;
            }

            _startTime = Time.time;
        }


        private void FixedUpdate()
        {
            float t = Time.time - _startTime;
            IsBlink = Mathf.Sin(t * SPEED) > 0f;
        }
    }
}
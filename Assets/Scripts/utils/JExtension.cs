﻿using UnityEngine;

namespace utils
{
    public static class JExtension
    {
        public static Vector3 ToSnapVector3(this Vector3 position)
        {
            float x = Mathf.FloorToInt(position.x);
            float z = Mathf.FloorToInt(position.z);
            return new Vector3(x, position.y, z);
        }
    }
}
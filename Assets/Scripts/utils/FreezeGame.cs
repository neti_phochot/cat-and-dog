﻿using System;
using System.Collections;
using UnityEngine;

namespace utils
{
    public class FreezeGame : MonoBehaviour
    {
        private static FreezeGame _instance;

        public static FreezeGame Instance
        {
            get
            {
                if (!_instance)
                {
                    _instance = new GameObject($"{nameof(FreezeGame)}").AddComponent<FreezeGame>();
                }

                return _instance;
            }
        }

        private void Awake()
        {
            if (_instance != null && _instance != this)
            {
                Destroy(gameObject);
                return;
            }
        }

        public static void Freeze(float value = 0f)
        {
            Time.timeScale = value;
        }


        public static void UnFreeze()
        {
            Time.timeScale = 1;
        }

        public void FreezeAction(float duration, Action enterAction = default, Action exitAction = default)
        {
            StartCoroutine(Freeze(duration, enterAction, exitAction));
        }

        private IEnumerator Freeze(float duration, Action enterAction, Action exitAction)
        {
            enterAction?.Invoke();
            Freeze();
            yield return new WaitForSecondsRealtime(duration);
            UnFreeze();
            exitAction?.Invoke();
        }
    }
}
﻿using UnityEngine;

namespace utils
{
    public class RotateOverTime : MonoBehaviour
    {
        [SerializeField] private float rotateSpeed;
        [SerializeField] private Vector3 direction;

        private void FixedUpdate()
        {
            transform.Rotate(direction * rotateSpeed * Time.fixedDeltaTime);
        }
    }
}
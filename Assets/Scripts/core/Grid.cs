using System;
using UnityEngine;

namespace core
{
    public class Grid<TGridObject>
    {
        public int Width { get; }
        public int Height { get; }
        public int CellSize { get; }
        public TGridObject[,] GridObjects { get; }

        public Grid(int width, int height, int cellSize,
            Func<Grid<TGridObject>, int, int, TGridObject> createGridObject)
        {
            Width = width;
            Height = height;
            CellSize = cellSize;

            GridObjects = new TGridObject[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    GridObjects[x, y] = createGridObject(this, x, y);
                }
            }
        }

        public TGridObject GetGetObject(int x, int y)
        {
            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                return GridObjects[x, y];
            }

            return default;
        }

        public TGridObject GetGridObjectFromWorldPosition(Vector3 position)
        {
            int x = Mathf.FloorToInt(position.x) / CellSize;
            int y = Mathf.FloorToInt(position.z) / CellSize;
            return GetGetObject(x, y);
        }
    }
}
﻿using game.world;
using UnityEngine;

namespace core.waypoint
{
    public class Waypoint : MonoBehaviour, ILocation
    {
        public Vector3 GetLocation()
        {
            return transform.position;
        }

        public void SetLocation(Vector3 location)
        {
            transform.position = location;
        }
    }
}
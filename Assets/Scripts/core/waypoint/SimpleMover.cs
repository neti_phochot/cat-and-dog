﻿using UnityEngine;

namespace core.waypoint
{
    public class SimpleMover : MonoBehaviour
    {
        [Header("MOVEMENT")] [SerializeField] private float moveSpeed = 10f;
        [SerializeField] private float turnSpeed = 3f;

        [Header("TRACKER")] [SerializeField] private float trackerMoveSpeedMultiplier = 1.1f;
        [SerializeField] private float maxAHeadDistance = 5f;

        [Header("WAYPOINT")] [SerializeField] private Waypoint[] waypoints;

        [Header("AUTO SPEED CONTROL")] [SerializeField]
        private bool autoSpeed;

        [SerializeField] private float maxFov = 35f;
        [SerializeField] private float increaseSpeed = 5f;
        [SerializeField] private float decreaseSpeed = 5f;

        private Waypoint _currentWaypoint;
        private int _waypointIndex;
        private Transform _tracker;

        private float _fixedMoveSpeed;

        private void Awake()
        {
            //Init first waypoint
            _currentWaypoint = waypoints[0];

            //Create Tracker
            _tracker = new GameObject($"{name}'s tracker").transform;
            _tracker.position = transform.position;

            //Init moves peed
            _fixedMoveSpeed = moveSpeed;
        }

        private void Update()
        {
            UpdateTrackerPosition();
            FollowTracker();
            FixedMoveSpeed();
        }

        private void UpdateTrackerPosition()
        {
            //Check if too far from player
            if (Vector3.Distance(_tracker.position, transform.position) > maxAHeadDistance)
            {
                return;
            }

            //Check if we reached current waypoint
            if (Vector3.Distance(_tracker.position, _currentWaypoint.GetLocation()) < 1f)
            {
                //Update next waypoint
                _currentWaypoint = waypoints[_waypointIndex++ % waypoints.Length];
            }

            //Move tracker
            float trackerMoveSpeed = _fixedMoveSpeed * trackerMoveSpeedMultiplier;
            _tracker.rotation = Quaternion.LookRotation(_currentWaypoint.GetLocation() - _tracker.position);
            _tracker.Translate(Vector3.forward * trackerMoveSpeed * Time.deltaTime);
        }

        private void FollowTracker()
        {
            Quaternion lookRotation = Quaternion.LookRotation(_tracker.position - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, turnSpeed * Time.deltaTime);
            transform.Translate(Vector3.forward * _fixedMoveSpeed * Time.deltaTime);
        }

        private void FixedMoveSpeed()
        {
            if (!autoSpeed)
            {
                _fixedMoveSpeed = moveSpeed;
                return;
            }

            if (GetFov() > maxFov)
            {
                float newSpeed = _fixedMoveSpeed - decreaseSpeed * Time.deltaTime;
                _fixedMoveSpeed = Mathf.Clamp(newSpeed, -moveSpeed, moveSpeed);
            }
            else
            {
                float newSpeed = _fixedMoveSpeed + increaseSpeed * Time.deltaTime;
                _fixedMoveSpeed = Mathf.Clamp(newSpeed, -moveSpeed, moveSpeed);
            }
        }

        private float GetFov()
        {
            Vector3 fromDirection = transform.forward;
            Vector3 toDirection = (_tracker.position - transform.position).normalized;
            float dot = Vector3.Dot(fromDirection, toDirection);
            float radian = Mathf.Acos(dot);
            float degree = radian * (180f / Mathf.PI);
            return degree;
        }

        #region DEBUG

        private Vector3 _offset = new Vector3(0, 0.2f);

        private void OnDrawGizmos()
        {
            if (!Application.isPlaying) return;
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(_tracker.position, 1f);
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, _tracker.position);

            for (int i = 0; i < waypoints.Length; i++)
            {
                Waypoint waypoint = waypoints[i];
                Waypoint nextWaypoint = waypoints[(i + 1) % waypoints.Length];
                Gizmos.color = Color.red;
                Gizmos.DrawLine(waypoint.GetLocation() + _offset, nextWaypoint.GetLocation() + _offset);
            }
        }

        #endregion
    }
}
﻿using UnityEngine;

namespace core.node
{
    public class PathNode : Node
    {
        public Grid<PathNode> Grid { get; }
        public float gCost;
        public float hCost;
        public float fCost;
        public PathNode cameFromNode;

        public bool isWalkable = true;

        public PathNode(Grid<PathNode> grid, int x, int y, bool isWalkable) : base(x, y)
        {
            Grid = grid;
            this.isWalkable = isWalkable;
        }

        public void CalculateFCost()
        {
            fCost = gCost + hCost;
        }

        public Vector3 ToWorldPosition()
        {
            return new Vector3(x * Grid.CellSize, 0f, y * Grid.CellSize);
        }

        public override string ToString()
        {
            return $"x:{x}, y:{y}";
        }
    }
}
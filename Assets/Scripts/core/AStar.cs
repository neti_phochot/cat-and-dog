﻿using System;
using System.Collections.Generic;
using core.node;
using UnityEngine;

namespace core
{
    public class AStar
    {
        public enum DirectionType
        {
            FOUR, //MANHATTAN
            EIGH, //DIAGONAL
        }

        private const int D = 10;
        private const int D2 = 14;

        private List<PathNode> _openList;
        private List<PathNode> _closeList;

        private readonly Grid<PathNode> _grid;

        public AStar(Grid<PathNode> grid)
        {
            _grid = grid;
        }

        public List<PathNode> FindPath(PathNode startNode, PathNode goalNode,
            DirectionType directionType = DirectionType.EIGH)
        {
            _openList = new List<PathNode> { startNode };
            _closeList = new List<PathNode>();

            foreach (var pathNode in _grid.GridObjects)
            {
                pathNode.cameFromNode = null;
                pathNode.gCost = int.MaxValue;
                pathNode.hCost = 0;
                pathNode.CalculateFCost();
            }

            startNode.gCost = 0;
            startNode.CalculateFCost();

            while (_openList.Count > 0)
            {
                PathNode currentNode = FindLowestFCost(_openList);

                if (currentNode == goalNode)
                {
                    return CalculatePathNode(currentNode);
                }

                _openList.Remove(currentNode);
                _closeList.Add(currentNode);

                foreach (var neighbourPathNode in FindNeighbourPathNodes(currentNode, directionType))
                {
                    if (_closeList.Contains(neighbourPathNode) || !neighbourPathNode.isWalkable) continue;

                    float tentativeGCost = currentNode.gCost + Heuristic(currentNode, neighbourPathNode, directionType);
                    if (tentativeGCost < neighbourPathNode.gCost)
                    {
                        neighbourPathNode.gCost = tentativeGCost;
                        neighbourPathNode.hCost = Heuristic(neighbourPathNode, goalNode, directionType);
                        neighbourPathNode.CalculateFCost();
                        neighbourPathNode.cameFromNode = currentNode;
                    }

                    if (!_openList.Contains(neighbourPathNode))
                    {
                        _openList.Add(neighbourPathNode);
                    }
                }
            }

            return new List<PathNode>();
        }


        private PathNode FindLowestFCost(List<PathNode> openList)
        {
            PathNode lowestFCostPathNode = _openList[0];
            foreach (var pathNode in openList)
            {
                if (pathNode.fCost < lowestFCostPathNode.fCost)
                {
                    lowestFCostPathNode = pathNode;
                }
            }

            return lowestFCostPathNode;
        }

        private List<PathNode> CalculatePathNode(PathNode goalPathNode)
        {
            List<PathNode> foundPathNodes = new List<PathNode> { goalPathNode };
            PathNode currentPathNode = goalPathNode;
            while (currentPathNode.cameFromNode != null)
            {
                foundPathNodes.Add(currentPathNode.cameFromNode);
                currentPathNode = currentPathNode.cameFromNode;
            }

            foundPathNodes.Reverse();
            return foundPathNodes;
        }

        private float Heuristic(PathNode a, PathNode b, DirectionType directionType)
        {
            //Source: http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html
            
            //D = 1
            //D2 = sqrt(2) > 1.41 
            
            int dx = Math.Abs(a.x - b.x);
            int dy = Mathf.Abs(a.y - b.y);

            return directionType == DirectionType.FOUR
                ?
                //4 directions
                //Manhattan Distance
                D * (dx + dy)
                :
                //8 directions
                //Diagonal Distance
                D * Mathf.Max(dx, dy) + (D2 - dx) * Mathf.Min(dx, dy);
        }

        private List<PathNode> FindNeighbourPathNodes(PathNode currentNode, DirectionType directionType)
        {
            bool isEightDirectionType = directionType == DirectionType.EIGH;
            List<PathNode> neighbourNodes = new List<PathNode>();
            if (currentNode.x - 1 >= 0)
            {
                //LEFT
                neighbourNodes.Add(_grid.GetGetObject(currentNode.x - 1, currentNode.y));

                if (isEightDirectionType)
                {
                    if (currentNode.y + 1 < _grid.Height)
                    {
                        //TOP LEFT
                        neighbourNodes.Add(_grid.GetGetObject(currentNode.x - 1, currentNode.y + 1));
                    }

                    if (currentNode.y - 1 >= 0)
                    {
                        //BOTTOM LEFT
                        neighbourNodes.Add(_grid.GetGetObject(currentNode.x - 1, currentNode.y - 1));
                    }
                }
            }

            if (currentNode.x + 1 < _grid.Width)
            {
                //RIGHT
                neighbourNodes.Add(_grid.GetGetObject(currentNode.x + 1, currentNode.y));

                if (isEightDirectionType)
                {
                    if (currentNode.y + 1 < _grid.Height)
                    {
                        //TOP RIGHT
                        neighbourNodes.Add(_grid.GetGetObject(currentNode.x + 1, currentNode.y + 1));
                    }

                    if (currentNode.y - 1 >= 0)
                    {
                        //BOTTOM RIGHT
                        neighbourNodes.Add(_grid.GetGetObject(currentNode.x + 1, currentNode.y - 1));
                    }
                }
            }

            if (currentNode.y + 1 < _grid.Height)
            {
                //TOP
                neighbourNodes.Add(_grid.GetGetObject(currentNode.x, currentNode.y + 1));
            }

            if (currentNode.y - 1 >= 0)
            {
                //BOTTOM
                neighbourNodes.Add(_grid.GetGetObject(currentNode.x, currentNode.y - 1));
            }

            return neighbourNodes;
        }
    }
}